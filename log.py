"""
Basic logging management
"""
__author__ = 'cgonzalez'

import sys
import time
import logging
import logging.handlers

from tempfile import NamedTemporaryFile


logger = logging.getLogger("dayenu")
logger_file = None
active_loggers = []

FILE = "file"
CONSOLE = "console"


def get_logger():
    """
    Return current logger
    :return:
    """
    global logger
    return logger


def set_loggers(loggers=(CONSOLE, ), prefix=""):
    """
    Configure application logger handlers
    :param loggers: list of active loggers in [CONSOLE, FILE]
    :return: configured logging.Logger object
    """
    global logger

    if (CONSOLE in loggers) and not(CONSOLE in active_loggers):
        # Console handler for debug purposes
        active_loggers.append(CONSOLE)
        formatter = logging.Formatter('[%(asctime)s][%(levelname)s][%(module)s] %(message)s')
        handler = logging.StreamHandler(sys.stderr)
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    if FILE in loggers and not(FILE in active_loggers):
        # Create a temporal file to store log
        active_loggers.append(FILE)
        log_localtime = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime(time.time()))
        f = NamedTemporaryFile(prefix=prefix+log_localtime+'_', suffix=".log",
                               delete=False)
        log_filename = f.name
        f.close()
        global logger_file
        logger_file = log_filename

        # File handler for production log
        formatter = logging.Formatter('[%(asctime)s][%(levelname)s][%(module)s] %(message)s')
        handler = logging.handlers.RotatingFileHandler(log_filename, maxBytes=200000, backupCount=10)
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    return logger


def set_logging_level(level):
    """
    Sets default logger level
    :param level: logging level
    :return: None
    """
    global logger
    logger.setLevel(level)


def get_logging_level():
    """
    Get default logger level
    :return: logging level
    """
    global logger
    return logger.getEffectiveLevel()

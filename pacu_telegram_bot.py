#!/usr/bin/env python3

import os
import log
import time
import json
import ephem
import RPi.GPIO as GPIO
from telegram.ext import Updater
from telegram.ext import CommandHandler, RegexHandler
from telegram import  *
from random import randint
from logging import DEBUG, INFO

logger = log.get_logger()


def door():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(4, GPIO.OUT)
    time.sleep(0.1)
    GPIO.setup(4, GPIO.IN)


def on_time():
    moment = time.gmtime(time.time())
    #print(moment)
    return start_h+dif <= moment.tm_hour < end_h+dif and 0 <= moment.tm_wday <= 4


def start_handler(bot, update):
    button_list = [[KeyboardButton("/suchai")], [KeyboardButton("/test")]]
    reply_markup = ReplyKeyboardMarkup(button_list)
    bot.send_message(update.message.chat_id, "Available commands", reply_markup=reply_markup)
    #bot.send_message(chat_id=update.message.chat_id, text="I'm a SPEL door bot, this are my commands\n/suchai - to get SUCHAI next pass\n/getID - to get your user ID")


def help_handler(bot, update):
    start_handler(bot, update)


def open_handler(bot, update):
    # bot.send_message(chat_id=update.message.chat_id, text="Authenticating user")
    user = update.effective_user
    # print(user)

    logger.info("Open. User: {}".format(user.name))  # Log all attempts
    
    if user['id'] in ids and on_time():
        bot.send_message(chat_id=update.message.chat_id, text="Authentication Ok, opening door")
        bot.send_message(chat_id=update.message.chat_id, text="Door /open")
        door()
    else:
        bot.send_message(chat_id=update.message.chat_id, text="Authentication Failed")


def suchai_handler(bot, update):
    """
    Get SUCHAI next pass over Santiago
    """
    # TODO: Update TLE (last 26-02-18)
    sat = ephem.readtle("SUCHAI","1 42788U 17036Z   18056.94548246  .00001357  00000-0  63129-4 0  9999","2 42788  97.4164 118.5297 0013738 105.2202 255.0554 15.21584607 37677")
    fcfm = ephem.city("Santiago")
    np = fcfm.next_pass(sat)
    message = "Next pass: {} UTC\nElevation: {}\nAzimut: {}".format(np[0], np[3], np[1])
    update.message.reply_text(message)

def test_handler(bot, update):
    #print("test")
    i = randint(0, len(emojis))
    print(emojis[i])
    update.message.reply_text(emojis[i])

def get_id_handler(bot, update):
    user = update.effective_user
    bot.send_message(chat_id=update.message.chat_id, text=str(user['id']))

    
if __name__ == "__main__":

    logger = log.set_loggers((log.FILE,), prefix="dorbot_")
    log.set_logging_level(INFO)
    
    with open(os.path.expanduser("~/.local/bin/bot.config"), 'r') as conf_file:
        conf = json.load(conf_file)

    ids = conf["ids"]
    dif = conf["time"]["dif"]
    start_h = conf["time"]["start"]
    end_h = conf["time"]["end"]
    logger.debug("Allowed IDs: {}".format(ids))

    emojis = [chr(i) for i in range(0x1F40C,0x1F43E)]

    updater = Updater(token=conf["token"])
    dispatcher = updater.dispatcher

    start_cmd_handler = CommandHandler('start', start_handler)
    help_cmd_handler = CommandHandler('help', help_handler)
    #open_cmd_handler = CommandHandler('open', open_handler)
    #getID_cmd_handler = CommandHandler('getID', get_id_handler)
    suchai_cmd_handler = CommandHandler('suchai', suchai_handler)
    test_cmd_handler = CommandHandler('test', test_handler)
    text_cmd_handler = RegexHandler(r'(?i)(hola|hi|hell|alo)\w*', test_handler)


    dispatcher.add_handler(start_cmd_handler)
    dispatcher.add_handler(help_cmd_handler)
    #dispatcher.add_handler(open_cmd_handler)
    #dispatcher.add_handler(getID_cmd_handler)
    dispatcher.add_handler(suchai_cmd_handler)
    dispatcher.add_handler(test_cmd_handler)
    dispatcher.add_handler(text_cmd_handler)


    try:
        updater.start_polling()
        updater.idle()
    except Exception as e:
        print(e)
        exit()

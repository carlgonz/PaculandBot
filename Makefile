install:
	cp pacu_telegram_bot.py ~/.local/bin
	cp log.py ~/.local/bin
	cp bot.config ~/.local/bin
	#mkdir ~/.local/share/systemd/user
	#mkdir ~/.local/bin
	cp bot.service ~/.local/share/systemd/user/
	systemctl --user daemon-reload
	systemctl --user enable bot.service
	systemctl --user restart bot.service
	systemctl --user status bot.service

clean:
	rm ~/.local/bin/pacu_telegram_bot.py
	rm ~/.local/bin/log.py
	rm ~/.local/bin/bot.config

dep-ubuntu:
	sudo apt-get install python3 python3-pip
	sudo pip3 install python-telegram-bot ephem
	
dep-arch:
	sudo pacman -S python python-pip python-setuptools
	sudo pip install python-telegram-bot ephem

all: clean install

.PHONY: all
